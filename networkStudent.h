//
//  networkStudent.hpp
//  C++ Project
//
//  Created by Tosha Williams on 1/6/24.
//

#ifndef networkStudent_h
#define networkStudent_h

#include <stdio.h>
#include "degree.h"
#include "student.h"

class networkStudent : public Student {
using Student::Student;
public:
virtual Degree getDegreeType();
private:
Degree degreeType = NETWORKING;
};


#endif /* networkStudent_hpp */

//
//  securityStudent.hpp
//  C++ Project
//
//  Created by Tosha Williams on 1/6/24.
//

#include <stdio.h>
#include "./student.h"


class securityStudent : public Student {

using Student::Student;
public:
virtual Degree getDegreeType();
private:
Degree degreeType = SECURITY;
};

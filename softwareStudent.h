//
//  softwareStudent.h
//  C++ Project
//
//  Created by Tosha Williams on 1/6/24.
//
#include <stdio.h>
#include "./student.h"


class softwareStudent : public Student {
using Student::Student;
public:
virtual Degree getDegreeType();
private:
Degree degreeType = SOFTWARE;
};
